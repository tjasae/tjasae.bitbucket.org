
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var sessionId = null;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    if (sessionId === null) {
        var response = $.ajax({
            type: "POST",
            url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                    "&password=" + encodeURIComponent(password),
            async: false
        });
        
        sessionId = response.responseJSON.sessionId;
    }
    return sessionId;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, ime, priimek, datumRojstva, telesnaVisina, telesnaTeza, telesnaTemperatura, krvniTlakSistolicni, krvniTlakDiastolicni, nasicenostKrvisKisikom) {
   var ehrId = null;
    // Najprej kreiramo EHR record
    $.ajax({
	    url: baseUrl + "/ehr", // pošiljanje zahteve
	    type: 'POST',
	    data: JSON.stringify({subjectId: stPacienta, subjectNamespace: 'tjasae'}),
        contentType: 'application/json',
        headers: {"Ehr-Session": getSessionId()},
	    async: false
    })
    .success(function (data) {
        var ehrId = data.ehrId; // ko dobimo odgovor
        // Nato pa mu dodamo naše podatke
        var partyData = {
            firstNames: ime,
            lastNames: priimek,
            partyAdditionalInfo: [
                {key: "ehrId", value: ehrId},
                {key: "datumRojstva", value: datumRojstva},
                {key: "telesnaVisina", value: telesnaVisina},
                {key: "telesnaTemperatura", value: telesnaTemperatura},
                {key: "telesnaTeza", value: telesnaTeza},
                {key: "krvniTlakSistolicni", value: krvniTlakSistolicni},
                {key: "krvniTlakDiastolicni", value: krvniTlakDiastolicni},
                {key: "nasicenostKrvisKisikom", value: nasicenostKrvisKisikom},
            ] // ehr id ko additional info
        };
        $.ajax({
            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
            type: 'POST',
            contentType: 'application/json',
            headers: {"Ehr-Session": getSessionId()},
            data: JSON.stringify(partyData),
            success: function (party) {  // uspešno
                if (party.action == 'CREATE') {
                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
                    $("#preberiEHRid").val(ehrId);
                    $('#seznam-pacientov').append($('<option>', {
                        value: ehrId,
                        text: ime + ' ' + priimek
                    }));
                }
            },
            error: function(err) {  // neuspešno
            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
            	console.log(JSON.parse(err.responseText).userMessage);
            }
        });
        
    });
    return ehrId;
}

function generiraj3() {
    // Generiramo tri nove preddefinirane paciente (raje ne klicat večkrat, ker bo potem polno istoimenskih pacientov v bazi)
    generirajPodatke($('#seznam-pacientov option').size(), 'Borut', 'Pahor', '2.11.1963', '179cm', '79kg', '36,6', '120', '80'); //idealni telesni znaki                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
    generirajPodatke($('#seznam-pacientov option').size(), 'Nina', 'Puslar', '25.10.1988', '169cm', '53kg', '38,4', '90', '60'); //pljučnica
    generirajPodatke($('#seznam-pacientov option').size(), 'Vid', 'Valic', '25.1.1982', '180cm', '54kg', '36,5'); //podhranjenost
}

function novPacient() {
    generirajPodatke($('#seznam-pacientov option').size(), $('#pacient-ime').val(), $('#pacient-priimek').val(), $('#pacient-datumRojstva').val(), 
              $('#pacient-telesnaVisina').val(), $('#pacient-telesnaTemperatura').val(), $('#pacient-telesnaTeza').val(), 
              $('#pacient-krvniTlakSistolicni').val(), $('#pacient-krvniTlakDiastolicni').val(), $('#pacient-nasicenostKrvisKisikom').val());
}
function popraviPacient() {

    var ehrId = $('#seznam-pacientov').val(); // ko dobimo odgovor
    // Nato pa mu dodamo naše podatke
    var partyData = {
        id: $('#pacient-id').val(),
        firstNames: $('#pacient-ime').val(),
        lastNames: $('#pacient-priimek').val(),
        partyAdditionalInfo: [
            {key: "ehrId", value: ehrId},
            {key: "datumRojstva", value: $('#pacient-datumRojstva').val()},
            {key: "telesnaVisina", value: $('#pacient-telesnaVisina').val()},
            {key: "telesnaTemperatura", value: $('#pacient-telesnaTemperatura').val()},
            {key: "telesnaTeza", value: $('#pacient-telesnaTeza').val()},
            {key: "krvniTlakSistolicni", value: $('#pacient-krvniTlakSistolicni').val()},
            {key: "krvniTlakDiastolicni", value: $('#pacient-krvniTlakDiastolicni').val()},
            {key: "nasicenostKrvisKisikom", value: $('#pacient-nasicenostKrvisKisikom').val()},
        ] // ehr id ko additional info
    };
    $.ajax({
        url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
        type: 'PUT',
        contentType: 'application/json',
        headers: {"Ehr-Session": getSessionId()},
        data: JSON.stringify(partyData),
        success: function (party) {  // uspešno
        },
        error: function(err) {  // neuspešno
        	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
        	console.log(JSON.parse(err.responseText).userMessage);
        }
    });

}

function beriPacienta(id) {
    // Prebere enega pacienta in ga doda v select na obrazcu, da ga uporabnik lahko izbere in prikaže njegove podatke
    var res = false;
    // Pregeremo ehrId iz našega IDja
    $.ajax({
        type: 'GET',
        url: baseUrl + '/ehr',
        data: {subjectId: id, subjectNamespace: 'tjasae'},
        headers: {"Ehr-Session": getSessionId()},
        async: false
    })
    
    .success(function(data) {
        if (data === undefined) {
            return;
        }
        var ehrId = data.ehrId;
        // Preberemo še podatke o pacientu
        $.ajax({
            type: 'GET',
            url: baseUrl + '/demographics/ehr/' + ehrId + '/party',
            headers: {"Ehr-Session": sessionId},
            async: false
        })
        .success(function(bolnik) {
            // Dodamo pacienta v select
            $('#seznam-pacientov').append($('<option>', {
                value: ehrId,
                text: bolnik.ime + ' ' + bolnik.priimek
            }));
            res = true;
        });
    });
    return res;
}

function preberiObstojecePaciente() {
    // Preberemo obstoječe zapise iz baze. Beremo dokler server še vrača zapise
    // !!! Zgleda, da tole ne dela. Videti je, da so vsi zapisi vezani le na trenutno sejo
    // Če temu ne bi bilo tako, bi tole naložilo paciente iz prejšnjih sej
    var i = 1;
    while (beriPacienta(i)) {
        i++;
    }
}

/*
Naloži podatke o pacientu v formo na ekranu
*/
function naloziPacienta(ehrId) {
    $.ajax({
        type: 'GET',
        url: baseUrl + '/demographics/ehr/' + ehrId + '/party',
        headers: {"Ehr-Session": sessionId},
        headers: {"Ehr-Session": getSessionId()},
        async: false
    })
    .success(function(bolnik) {
        $('#pacient-id').val(bolnik.party.id);
        $('#pacient-ime').val(bolnik.party.firstNames);
        $('#pacient-priimek').val(bolnik.party.lastNames);
        $.each(bolnik.party.partyAdditionalInfo, function(idx, data) {
            $('#pacient-' + data.key).val(data.value);
            if (data.key == 'krvniTlakSistolicni') {
                krvniTlakSistolicni = Number(data.value);
            }
              
        });
        generateUniformHistogram(krvniTlakSistolicni);
    });
}

generateUniformHistogram = function(krvniTlakSistolicni) {
    var data = [
        { pritisk: 85, pm: 90, stevilo: 0.3},
        { pritisk: 90, pm: 100, stevilo: 0.5},
        { pritisk: 100, pm: 110, stevilo: 2.6},
        { pritisk: 110, pm: 120, stevilo: 8.4},
        { pritisk: 120, pm: 130, stevilo: 12.1},
        { pritisk: 130, pm: 140, stevilo: 8},
        { pritisk: 140, pm: 150, stevilo: 4.1},
        { pritisk: 150, pm: 160, stevilo: 3.7},
        { pritisk: 160, pm: 170, stevilo: 0.9},
        { pritisk: 170, pm: 990, stevilo: 0.4}
        ];
    
    var svg = d3.select("svg"),
        margin = {
            top: 10,
            right: 30,
            bottom: 30,
            left: 30
        },
        width = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom,
        g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var x = d3.scaleLinear().domain([85, 180]).range([0, width]);
    var y = d3.scaleLinear().domain([0, 14]).range([height, 0]);
    var formatCount = d3.format(",.0f");
    
    var bar = g.selectAll(".bar")
        .data(data)
        .enter().append("g")
        .attr("class", function(d) {
            return d.pritisk <= krvniTlakSistolicni && d.pm > krvniTlakSistolicni ? "bar barsel" : "bar";
        })
        .attr("transform", function(d) {
            return "translate(" + x(d.pritisk) + "," + y(d.stevilo) + ")";
        });
    
    bar.append("rect")
        .attr("x", 1)
        .attr("width", x(data[0].pm) - x(data[0].pritisk))
        .attr("height", function(d) {
            return height - y(d.stevilo);
        });
    
    bar.append("text")
        .attr("dy", ".75em")
        .attr("y", 6)
        .attr("x", (x(data[0].pm) - x(data[0].pritisk)) / 2)
        .attr("text-anchor", "middle")
        .text(function(d) {
            return formatCount(d.stevilo);
        });
    
    g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    
}
